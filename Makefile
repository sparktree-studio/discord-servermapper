IMAGE=sparktree/vespucci-bot

_PHONY: up dev down build_dev build_prod
up:
	docker compose -f docker/docker-compose.yml up -d

dev:
	docker compose -f docker/docker-compose.yml up

down:
	docker compose -f docker/docker-compose.yml down -v


build_dev:
	docker build --target dev ${IMAGE}:dev