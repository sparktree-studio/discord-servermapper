import { SlashCommandBuilder } from '@discordjs/builders';
import { loadTemplate, tOpts } from "../template.js";
import { buildCategoryChannelList } from '../utils.js';

const map = async (interaction) => {
    var categories = await buildCategoryChannelList(interaction);

    var t = await loadTemplate("./templates/server_map.handlebars")
    var rendered = t({
        "guild": interaction.guild,
        "categories": categories },
        tOpts
    );
    return rendered
}

export const data = new SlashCommandBuilder()
    .setName('map_server')
    .setDescription('Generates a server map')
    .addChannelOption(
        option => option
            .setName('channel')
            .setDescription('Category to build map for')
    );

export async function execute(interaction) {
    const channel = interaction.options.getChannel(
        'channel'
    );

    var out = await map(interaction);
    if (channel) {
        await channel.send(out);
    } else {
        await interaction.reply(out);
    }
}
