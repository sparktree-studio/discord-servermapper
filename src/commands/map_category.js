import { SlashCommandBuilder } from '@discordjs/builders';
import { loadTemplate, tOpts } from "../template.js";
import { buildCategoryChannelMap } from '../utils.js';

const map_category = async (interaction, channel) => {
    var categories = await buildCategoryChannelMap(interaction);
    var category = categories[channel.name]

    var t = await loadTemplate("./templates/category_map.handlebars")
    var rendered = t({ "category": category}, tOpts)
    return rendered
}

export const data = new SlashCommandBuilder()
    .setName('map_category')
    .setDescription(
        'Generates a channel map for a ' +
        'particular category'
    )
    .addChannelOption(
        option => option
            .setName('category_channel')
            .setDescription('Category to build map for')
            .setRequired(true)
    );


export async function execute(interaction) {
    const category_channel = interaction.options.getChannel(
        'category_channel'
    );

    var out = await map_category(interaction, category_channel);
    await interaction.reply(out);
}
