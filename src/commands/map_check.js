import { SlashCommandBuilder } from '@discordjs/builders';
import { loadTemplate, tOpts } from "../template.js";
import { buildCategoryChannelList } from '../utils.js';

const map_review = async (interaction) => {
    var categories = await buildCategoryChannelList(interaction);

    var t = await loadTemplate("./templates/server_map_check.handlebars")
    var rendered = t({ "guild": interaction.guild, "categories": categories }, tOpts)
    return rendered
}

export const data = new SlashCommandBuilder()
    .setName('map_review')
    .setDescription(
        'Check channels for topics, etc before' +
        ' generating a server map'
    );

export async function execute(interaction) {
    var out = await map_review(interaction);
    await interaction.reply(out);
}