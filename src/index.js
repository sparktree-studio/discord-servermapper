import { readdirSync } from 'node:fs';
import { Client, Collection, Intents } from 'discord.js';

const TOKEN = process.env.TOKEN
const CLIENT_ID = process.env.CLIENT_ID
const GUILD_ID = process.env.GUILD_ID

// Create a new client instance
const client = new Client({
    intents: [
        Intents.FLAGS.GUILDS,
        Intents.FLAGS.GUILD_MESSAGES,
        Intents.FLAGS.GUILD_MESSAGE_REACTIONS
    ] });

client.commands = new Collection();

console.log("Command setup...")
const commandFiles = readdirSync('./commands').filter(file => file.endsWith('.js'));
for (const file of commandFiles) {
    console.debug(`Loading ${file}`)
    const command = await import(`./commands/${file}`);
    console.debug(command)
    console.debug(`Successfully loaded ${command.data.name} from ${file}`)
    client.commands.set(command.data.name, command);
}


client.once('ready', c => {
	console.log(`Bot ready, logged in as ${c.user.tag}`);
});


client.on('interactionCreate', async interaction => {
    if (!interaction.isCommand()) return;

    const command = client.commands.get(interaction.commandName);

    try {
        await command.execute(interaction)
    } catch (error) {
        console.error(error);
        await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
    }
});

// Login to Discord with your client's token
client.login(TOKEN);