import {
    readFile,
} from 'fs/promises';

import pkg from 'handlebars';
// console.debug(pkg)

const Handlebars = pkg;

Handlebars.registerHelper('upper', function(aString) {
    return aString.toUpperCase();
});

Handlebars.registerHelper('ifNot', function(value, options) {
    if (!value) { return options.fn(this); }
});

export const tOpts = {
    allowProtoPropertiesByDefault: true,
    allowProtoMethodsByDefault: true
}

export const loadTemplate = async (path) => {
    var tmplString = "";

    try {
        var tmplString = await readFile(path, 'utf8');
    } catch (error) {
        console.error(error);
        return null;
    }

    var template = Handlebars.compile(tmplString);
    return template
}