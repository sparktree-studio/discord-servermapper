export const buildCategoryChannelList = async (interaction, category_name = null) => {
    var categories = [];

    var catList = await interaction.guild.channels.fetch();
    for (const c of catList.values()) {
        if (c.type == 'GUILD_CATEGORY') {
            const channels = Array.from(c.children.values())
            categories.push({
                name: c.name,
                channels: channels
            })
        }
    }
    return categories;
}

export const buildCategoryChannelMap = async (interaction, category_name = null) => {
    var categories = {};

    var catList = await interaction.guild.channels.fetch();
    for (const c of catList.values()) {
        if (c.type == 'GUILD_CATEGORY') {
            const channels = Array.from(c.children.values())
            categories[c.name] = {
                name: c.name,
                channels: channels
            }
        }
    }
    return categories;
}
